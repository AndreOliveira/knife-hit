﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    [SerializeField] private GameObject prefab = null;
    [SerializeField] private int amount = 10;
    private GameObject[] pool;

    protected GameObject[] Pool { get => pool; }

    protected virtual void Start()
    {
        pool = new GameObject[amount];

        for (int i = 0; i < amount; i++)
        {
            GameObject pooled = Instantiate<GameObject>(prefab, transform);
            pooled.SetActive(false);
            pool[i] = pooled;
        }
    }

    public GameObject Spawn(Vector3 pos, Quaternion rot, Vector3 scale)
    {
        for (int i = 0; i < pool.Length; i++)
        {
            if (!pool[i].activeInHierarchy)
            {
                pool[i].transform.position = pos;
                pool[i].transform.rotation = rot;
                pool[i].transform.localScale = scale;
                pool[i].SetActive(true);
                return pool[i];
            }
        }
        return null;
    }

    public GameObject Spawn(Vector3 pos)
    {
        return Spawn(pos, Quaternion.identity, Vector3.one);
    }

    public T Spawn<T>(Vector3 pos)
    {
        return Spawn(pos, Quaternion.identity, Vector3.one).GetComponent<T>();
    }

    public T Spawn<T>(Vector3 pos, Quaternion rot, Vector3 scale)
    {
        return Spawn(pos, rot, scale).GetComponent<T>();
    }
}
