﻿using UnityEngine;
using DG.Tweening;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameManager manager = null;
    [SerializeField] private ScriptableSettings settings = null;
    [SerializeField] private CanvasGroup[] screens = null;
    [SerializeField] private CanvasGroup mainMenuGroup = null;

    public void Start()
    {
        mainMenuGroup.alpha = 0f;
        mainMenuGroup.DOFade(1f, settings.Duration.Medium);

        //setup screens
        for (int i = 0; i < screens.Length; i++)
        {
            screens[i].alpha = 0f;
            RectTransform rectTransf = screens[i].GetComponent<RectTransform>();
            rectTransf.offsetMin = Vector2.zero;
            rectTransf.offsetMax = Vector2.zero;
            screens[i].blocksRaycasts = false;
        }

        screens[0].blocksRaycasts = true;
        screens[0].DOFade(1f, settings.Duration.Medium)
            .SetEase(Ease.InOutQuad);
    }

    public void ActiveScreen(CanvasGroup screen)
    {
        for (int i = 0; i < screens.Length; i++)
        {
            if (screens[i].alpha != 1f) continue;

            screens[i].blocksRaycasts = false;
            screens[i].DOFade(0f, settings.Duration.Medium)
                .SetEase(Ease.InOutQuad)
                .OnComplete(() =>
                {
                    screen.blocksRaycasts = true;
                    screen.DOFade(1f, settings.Duration.Medium)
                    .SetEase(Ease.InOutQuad);
                });
            return;
        }
    }

    public void Play()
    {
        screens[0].blocksRaycasts = false;
        screens[0].DOFade(0f, settings.Duration.Medium)
            .SetEase(Ease.InOutQuad);

        mainMenuGroup.DOFade(0f, settings.Duration.Medium)
            .SetEase(Ease.InOutQuad);

        manager.State = GameState.GAMEPLAY;
    }

    public void Quit()
    {
        Application.Quit();
    }

}
