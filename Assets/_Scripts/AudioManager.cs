﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
	//public static AudioManager Instance { get; private set; }

	[Header("General")]
	[SerializeField] private AudioMixer mixer = null;
	[SerializeField] private ScriptableSettings settings = null;

	[Header("Music")]
	[SerializeField] private AudioSource musicSource;
	[SerializeField] private AudioClip gameplayClip = null;
	[SerializeField] private AudioClip menuClip = null;

	[Header("SFX")]
	[SerializeField] private AudioSource sfxSource;

    private void GameplayMusic()
    {
		mixer.DOSetFloat(Constants.Key.MUSIC, 0f, settings.Duration.VeryLong / 2f)
			.SetEase(Ease.InOutQuad)
			.OnComplete(() =>
			{
				musicSource.clip = gameplayClip;
				mixer.DOSetFloat(Constants.Key.MUSIC, 1f, settings.Duration.VeryLong / 2f)
			   .SetEase(Ease.InOutQuad);
			});
    }
}