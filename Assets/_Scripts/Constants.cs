﻿public static class Constants
{
    public static class Key
    {
        public static readonly string MUSIC = "MUSIC";
        public static readonly string SFX = "SFX";
    }
}
