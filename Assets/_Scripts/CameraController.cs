﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 startPosition;
    private Quaternion startRotation;
    [SerializeField] private Transform gameplayCameraTransform = null;
    [SerializeField] private ScriptableSettings settings = null;

    void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

    public void GoToGameplayPosition()
    {
        StartCoroutine(Move(gameplayCameraTransform.position));
        StartCoroutine(Rotate(gameplayCameraTransform.rotation));
    }

    public void GoToMenuPosition()
    {
        StartCoroutine(Move(startPosition));
        StartCoroutine(Rotate(startRotation));
    }

    private IEnumerator Move(Vector3 destiny)
    {
        float percent = 0;

        Vector3 originalPos = transform.position;

        while (percent < 1f)
        {
            percent += Time.fixedDeltaTime / settings.Duration.VeryLong;
            transform.position = Vector3.Slerp(originalPos, destiny, percent);
            yield return new WaitForFixedUpdate();
        }
    }

    private IEnumerator Rotate(Quaternion destiny)
    {
        float percent = 0;

        Quaternion originalRot = transform.rotation;

        while (percent < 1f)
        {
            percent += Time.fixedDeltaTime / settings.Duration.VeryLong;
            transform.rotation = Quaternion.Slerp(originalRot, destiny, percent);
            yield return new WaitForFixedUpdate();
        }
    }
}
