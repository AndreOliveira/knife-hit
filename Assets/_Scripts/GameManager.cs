﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameState state;
    [SerializeField] private ScriptableSettings settings = null;
    [SerializeField] private CameraController camController = null;
    [SerializeField] private AudioManager audioManager = null;
    [SerializeField] private MainMenu menu = null;
    [SerializeField] private Gameplay gameplay = null;

    public GameState State
    {
        get => state;
        set
        {
            switch (value)
            {
                case GameState.MENU:
                    StartCoroutine(ActiveMenu());
                    return;
                case GameState.GAMEPLAY:
                    StartCoroutine(ActiveGame());
                    return;
                case GameState.GAMEOVER:
                    return;
            }
        }
    }

    private IEnumerator ActiveMenu()
    {
        camController.GoToMenuPosition();
        yield return new WaitForSeconds(settings.Duration.VeryLong);
        menu.Start();
    }
    private IEnumerator ActiveGame()
    {
        yield return new WaitForSeconds(settings.Duration.Medium);
        camController.GoToGameplayPosition();
        yield return new WaitForSeconds(settings.Duration.VeryLong);
        //gameplay.Begin();
    }

}

public enum GameState
{
    MENU,
    GAMEPLAY,
    GAMEOVER
}
