﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private ScriptableSettings settings = null;
    [SerializeField] private Slider musicSlider = null;
    [SerializeField] private Slider sfxSlider = null;
    [SerializeField] private AudioMixer mixer = null;

    private void Start()
    {
        Load();

        //setup music
        musicSlider.value = settings.MusicVolume;
        musicSlider.onValueChanged.AddListener(SetMusicVolume);

        //setup sfx
        sfxSlider.value = settings.SfxVolume;
        sfxSlider.onValueChanged.AddListener(SetSFXVolume);

        UpdateAudio();
    }

    public void Save()
    {
        PlayerPrefs.SetFloat(Constants.Key.MUSIC, settings.MusicVolume);
        PlayerPrefs.SetFloat(Constants.Key.SFX, settings.SfxVolume);
    }

    private void Load()
    {
        bool hasMusicKey = PlayerPrefs.HasKey(Constants.Key.MUSIC);
        bool hasSFXKey = PlayerPrefs.HasKey(Constants.Key.SFX);

        settings.MusicVolume = hasMusicKey ? PlayerPrefs.GetFloat(Constants.Key.MUSIC) : 1f;
        settings.SfxVolume = hasSFXKey ? PlayerPrefs.GetFloat(Constants.Key.SFX) : 1f;
    }

    #region audio handler
    private void SetMusicVolume(float f)
    {
        float vol = f / musicSlider.maxValue;
        settings.MusicVolume = Mathf.Clamp(vol, Mathf.Epsilon, 1f);

        UpdateAudio();
    }

    private void SetSFXVolume(float f)
    {
        float vol = f / sfxSlider.maxValue;
        settings.SfxVolume = Mathf.Clamp(vol, Mathf.Epsilon, 1f);

        UpdateAudio();
    }
    public void UpdateAudio()
    {
        float music = Mathf.Log10(settings.MusicVolume) * 20f;
        mixer.SetFloat(Constants.Key.MUSIC, music);

        float sfx = Mathf.Log10(settings.SfxVolume) * 20f;
        mixer.SetFloat(Constants.Key.SFX, sfx);
    }
    #endregion
}
