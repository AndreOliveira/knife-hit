﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "Scriptable Settings", order = 0)]
public class ScriptableSettings : ScriptableObject
{
    [SerializeField] private Duration duration = new Duration();
    [SerializeField] private float musicVolume = 1f;
    [SerializeField] private float sfxVolume = 1f;

    public Duration Duration { get => duration; set => duration = value; }
    public float MusicVolume { get => musicVolume; set => musicVolume = value; }
    public float SfxVolume { get => sfxVolume; set => sfxVolume = value; }
}

[Serializable]
public class Duration
{
    [SerializeField] private float @short = 0.25f;
    [SerializeField] private float medium = 0.5f;
    [SerializeField] private float @long = 1f;
    [SerializeField] private float veryLong = 3f;

    public float Short { get => @short; }
    public float Medium { get => medium; }
    public float Long { get => @long; }
    public float VeryLong { get => veryLong; }
}
