﻿using UnityEngine;

[CreateAssetMenu(fileName ="Float", menuName ="Scriptable Primitives/Float", order = 0)]
public class ScriptableFloat : ScriptableObject
{
    public delegate void OnValueChange(float newValue);
    public OnValueChange onValueChange;

    private float value;

    public float Value {
        get => value;
        set
        {
            this.value = value;
            onValueChange?.Invoke(this.value);
        }
    }
}
